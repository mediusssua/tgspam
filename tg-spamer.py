from telethon import TelegramClient, sync, events
from time import sleep
import random

from telethon.errors import PeerFloodError
from telethon.tl.functions.messages import SendMessageRequest


api_id = 7799586
api_hash = 'c0fa545348116ee161e10719653dd3f9'
phone = "+380933902396"

client = TelegramClient('Spam-session', api_id, api_hash)
client.start()
client.connect()
if not client.is_user_authorized():
    client.send_code_request(phone)
    client.sign_in(phone, input('Введите полученный код: '))

usernames = open('tg-usernames.txt', 'r')


def spam():
    for username in usernames:
        try:
            client(SendMessageRequest(username, random.choice(open('input-text.txt', encoding='utf8').readlines())))
            sleep(30)
        except PeerFloodError:
            print("Cлишком много запросов к Телеграму.")
            sleep(30)
            continue
        except Exception as e:
            sleep(30)
            print("Ошибка:", e)
            print("Пытаемся продолжить...")
            continue


spam()
print("All OK")
